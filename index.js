let _horses = [];
let _user = user();
let _time = 2000;

function horse(name)
{
    this.name = name;
    this.wage = 0;
    this.whenFinished = 0;
    this.run = function () {return new Promise((resolve, rejected) => {
        let time = Math.floor(1 + Math.random() * (_time));
        setTimeout(() => { 
            resolve({ name: this.name, whenFinished: time, wage: this.wage}); }, time);
    });
}
    return this;
}

function user()
{
    this.account = 100;
    return this;
}

function showHorses()//вывод списка лошадей
{
    for (let i = 0; i < _horses.length; i++)
    {
        console.log(_horses[i].name);
    }  
}
function showAccount()//счёт игрока
{
    console.log(_user.account);
}
function setWager(name, wage)//сделать ставку на лошадь в следующем забеге
{
    try{
        _user.account -= wage;
        if (_user.account < 0){
            _user.account += wage;
            throw new Error('У вас недостаточно средств!');
        }
    }
    catch(err){
        console.log(err.message);
    }
    finally{
    for (let i = 0; i < _horses.length; i++)
    {
        if (_horses[i].name === name)
        {
            _horses[i].name = name;
            _horses[i].wage = wage;
        }
    }  
    }
}

function startRacing()//начало забега
{
    const promiseArr = _horses.map((horse) => {
        return horse.run()
      })
      
    _horses.map((horse) => {
        promiseArr.push(horse.run());
    })
    
   try{
    Promise.race(promiseArr).then(firstHorse => {
		console.log('Первая лошадь:', firstHorse.name)
		firstHorse.isWinner = true;
        })
    Promise.all(promiseArr).then(promises => {
        promises.sort((a, b) => a.whenFinished > b.whenFinished ? 1 : -1);
        let i = 0;
        promises.map((horse) =>{
        console.log(`${horse.name} - ${horse.whenFinished}`);
        this.account += horse.wage/(i+1);
        console.log(`Вы выиграли ${horse.wage/(i+1)}`);
        })
    })
    }
    catch (err){
        return 'error';
    }
}
function newGame()//новая игра
{
    let count = prompt('Сколько участвует лошадей?', 2);
    for (let i = 0; i < count; i++){
        let name = prompt(`Введите имя лошади №${i + 1}`);
        let _horse = new horse(name);
        _horses.push(_horse);
    }
}
